package service;

import dao.ElasticSearchConnection;
import entity.*;

import javax.inject.Inject;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class OperationService {

//    @Inject
    ElasticSearchConnection elasticSearchConnection = new ElasticSearchConnection();

    private List<LocalInformation> localInformationList = new ArrayList<LocalInformation>();
    private List<ExternalInformation> externalInformationList = new ArrayList<>();
    private List<OutputInformation> outputInformationList = new ArrayList<>();
    private ConnectionWrapper connectionWrapper;

    public void init(InitializationParameter initializationParameter){
        String[] urlData = initializationParameter.getElasticUrl().split(":");
        connectionWrapper = new ConnectionWrapper(urlData[0],Integer.getInteger(urlData[1]),"http",initializationParameter.getIndexName());
        comparingData(initializationParameter.getFieldName());
    }

    public void comparingData(String compareField){
        try {
            String localMethodName = new PropertyDescriptor(compareField, LocalInformation.class).getReadMethod().getName();
            Method localMethod = LocalInformation.class.getMethod(localMethodName);

            String externalMethodName = new PropertyDescriptor(compareField, ExternalInformation.class).getReadMethod().getName();
            Method externalMethod = ExternalInformation.class.getMethod(externalMethodName);

            localInformationList = findLocalInformation();
            for (LocalInformation localInformation : localInformationList){
                for (ExternalInformation externalInformation : externalInformationList){
                    if (localMethod.invoke(localInformation).equals(externalMethod.invoke(externalInformation))){
                        OutputInformation outputInformation = new OutputInformation();
                        outputInformation.setIp(localInformation.getIp());
                        outputInformation.setUsername(localInformation.getUserName());
                        outputInformation.setRegisterTime(localInformation.getRegisterTime());
                        outputInformation.setName(externalInformation.getName());
                        outputInformationList.add(outputInformation);
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public List<LocalInformation> findLocalInformation(){
        Date firstDate = new GregorianCalendar(1970, Calendar.JANUARY, 1,0,0,0).getTime();
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date twentyDaysBefore = new Date(System.currentTimeMillis() - (20 * DAY_IN_MS));
        long difference =  (twentyDaysBefore.getTime()-firstDate.getTime());
        return elasticSearchConnection.findInformationListByTime(connectionWrapper,String.valueOf(difference));
    }

}
