package service;

import entity.InitializationParameter;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("/service")
public class RestApiService {

    @Inject
    OperationService operationService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void calling(InitializationParameter initializationParameter) {
        operationService.init(initializationParameter);
        System.out.println("gh");
    }

    @GET
    @Produces("text/plain")
    public String getClichedMessage() {
        return "Hello World";
    }
}
