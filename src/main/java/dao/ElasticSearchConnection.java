package dao;

import com.google.gson.Gson;
import entity.ConnectionWrapper;
import entity.LocalInformation;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ElasticSearchConnection {

    private static RestHighLevelClient client;

    public static synchronized RestHighLevelClient makeConnection(ConnectionWrapper connectionWrapper) {
        client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(connectionWrapper.getHost(), connectionWrapper.getPortNo(), connectionWrapper.getScheme())));

        return client;
    }

    public static synchronized void closeConnection() throws IOException {
        client.close();
        client = null;
    }

    public static List<LocalInformation> findInformationListByTime(ConnectionWrapper connectionWrapper, String timeLimit){
        try {
            makeConnection(connectionWrapper);
            SearchRequest searchRequest = new SearchRequest(connectionWrapper.getIndex());
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
             searchSourceBuilder.query(QueryBuilders.rangeQuery("registerTime").from(timeLimit));
            searchRequest.source(searchSourceBuilder);
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            List<LocalInformation> localInformationList = new ArrayList<>();
            Arrays.stream(searchResponse.getHits().getHits()).forEach(hit ->
                    localInformationList.add(new Gson().fromJson(hit.getSourceAsString(), LocalInformation.class)));
            closeConnection();
            return localInformationList;
        } catch (java.io.IOException e){
            e.getLocalizedMessage();
            return new ArrayList<>();
        }
    }

}
