package entity;

import java.util.Objects;

public class ExternalInformation {

    private String ip;
    private Long registerTime;
    private String name;

    public ExternalInformation(String ip, Long registerTime, String name) {
        this.ip = ip;
        this.registerTime = registerTime;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalInformation that = (ExternalInformation) o;
        return Objects.equals(ip, that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip);
    }

    /* getter and setter */
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getRegisterTime() {
        return registerTime;
    }
    public void setRegisterTime(Long registerTime) {
        this.registerTime = registerTime;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
