package entity;

public class ConnectionWrapper {

    private String host;
    private int portNo;
    private String scheme;
    private String index;

    public ConnectionWrapper(String host, int portNo, String scheme, String index) {
        this.host = host;
        this.portNo = portNo;
        this.scheme = scheme;
        this.index = index;
    }

    /* getter and setter */
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }

    public int getPortNo() {
        return portNo;
    }
    public void setPortNo(int portNo) {
        this.portNo = portNo;
    }

    public String getScheme() {
        return scheme;
    }
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
}
