package entity;

public class InitializationParameter {

    private String kafkaUrl;
    private String inputTopic;
    private String outputTopic;
    private String elasticUrl;
    private String indexName;
    private String fieldName;

    /* getter and setter */
    public String getKafkaUrl() {
        return kafkaUrl;
    }
    public void setKafkaUrl(String kafkaUrl) {
        this.kafkaUrl = kafkaUrl;
    }

    public String getInputTopic() {
        return inputTopic;
    }
    public void setInputTopic(String inputTopic) {
        this.inputTopic = inputTopic;
    }

    public String getOutputTopic() {
        return outputTopic;
    }
    public void setOutputTopic(String outputTopic) {
        this.outputTopic = outputTopic;
    }

    public String getElasticUrl() {
        return elasticUrl;
    }
    public void setElasticUrl(String elasticUrl) {
        this.elasticUrl = elasticUrl;
    }

    public String getIndexName() {
        return indexName;
    }
    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
